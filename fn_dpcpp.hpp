// DPC++ implementation
#include <CL/sycl.hpp>
#define THREADS_PER_SITE 48

using namespace cl::sycl;

// SYCL lambda function kernel names
class make_back;
class dslash;

double dslash_fn(
  const std::vector<su3_vector> &src, 
        std::vector<su3_vector> &dst,
  const std::vector<su3_matrix> &fat,
  const std::vector<su3_matrix> &lng,
        std::vector<su3_matrix> &fatbck,
        std::vector<su3_matrix> &lngbck,
  size_t *fwd, size_t *bck, size_t *fwd3, size_t *bck3,    
  const size_t iterations,
  size_t wgsize )
{ 
  // Set device and queue
  default_selector selector;
  queue queue(selector);
  
  if (verbose > 0) {
    // Print device information
    std::cout << "Getting device info: ";
    std::cout << "Device " << selector.select_device().get_info<info::device::name>() \
	            << ": Driver " << selector.select_device().get_info<info::device::driver_version>() << std::endl;
    std::cout << std::flush;
  }

  // Pre-build the dslash kernel
  auto build_start = Clock::now();
  program program1 = program(queue.get_context());
  program1.build_with_kernel_type<dslash>();
  double build_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-build_start).count();
  if (verbose > 1) {
    std::cout << "Time to build kernel = " << build_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }
  
  // Set the loop and work-group parameters
  if (wgsize == 0)
    wgsize = THREADS_PER_SITE;
  
  size_t total_sites = sites_on_node; 
  size_t total_even_sites = even_sites_on_node;
  
  if (verbose > 1) {
    std::cout << "max compute units = " 
	            << queue.get_device().get_info<info::device::max_compute_units>() << "\n";
    std::cout << "max workgroup size = " 
	            << queue.get_device().get_info<info::device::max_work_group_size>() << "\n";
    std::cout << std::flush;
  }
  
  auto copy_start = Clock::now();
  // allocate device memory
  su3_vector* d_src    = (su3_vector*) malloc_device(total_sites * 1 * sizeof(su3_vector), queue);
  su3_vector* d_dst    = (su3_vector*) malloc_device(total_sites * 1 * sizeof(su3_vector), queue);
  su3_matrix* d_fat    = (su3_matrix*) malloc_device(total_sites * 4 * sizeof(su3_matrix), queue);
  su3_matrix* d_lng    = (su3_matrix*) malloc_device(total_sites * 4 * sizeof(su3_matrix), queue);
  su3_matrix* d_fatbck = (su3_matrix*) malloc_device(total_sites * 4 * sizeof(su3_matrix), queue);
  su3_matrix* d_lngbck = (su3_matrix*) malloc_device(total_sites * 4 * sizeof(su3_matrix), queue);
  if (    d_src == NULL    || d_dst == NULL 
       || d_fat == NULL    || d_lng == NULL
       || d_fatbck == NULL || d_lngbck == NULL ) {
    std::cout << "Unable to allocate device memory " << std::endl;
    exit(1);
  }

  size_t* d_fwd  = (size_t*) malloc_device(total_sites * 4 * sizeof(size_t), queue);
  size_t* d_bck  = (size_t*) malloc_device(total_sites * 4 * sizeof(size_t), queue);
  size_t* d_fwd3 = (size_t*) malloc_device(total_sites * 4 * sizeof(size_t), queue);
  size_t* d_bck3 = (size_t*) malloc_device(total_sites * 4 * sizeof(size_t), queue);
  
  size_t bytes  = sizeof(size_t)*total_sites*4;
  auto e_fat  = queue.memcpy(d_fat, fat.data(), fat.size() * sizeof(su3_matrix));
  auto e_lng  = queue.memcpy(d_lng, lng.data(), lng.size() * sizeof(su3_matrix));
  auto e_bck  = queue.memcpy(d_bck, bck, bytes);
  auto e_bck3 = queue.memcpy(d_bck3, bck3, bytes);

  auto e_src  = queue.memcpy(d_src, src.data(), src.size() * sizeof(su3_vector));
  auto e_fwd  = queue.memcpy(d_fwd, fwd, bytes);
  auto e_fwd3 = queue.memcpy(d_fwd3, fwd3, bytes);

  double copy_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-copy_start).count();
  if (verbose > 1) {
    std::cout << "Time to offload input data = " << copy_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }

  // Create backward links on the device
  size_t total_wi = total_even_sites;
  if (verbose > 1) {
    std::cout << "Creating backward links"  << std::endl;
    std::cout << "Setting number of work items " << total_wi << std::endl;
    std::cout << "Setting workgroup size to " << 1 << std::endl;
  }
  auto back_start = Clock::now();
  auto e_make_back = queue.submit( [&](handler& cgh) {
    cgh.depends_on({e_fat, e_lng, e_bck, e_bck3});
    // Lambda function defines the kernel scope
    cgh.parallel_for<class make_back>(
	  nd_range<1> {total_wi, 1}, [=](nd_item<1> item) {
      size_t myThread = item.get_global_id(0);
	    size_t mySite = myThread;  // Do only even site
	    if (mySite < total_even_sites) {
	      for(int dir = 0; dir < 4; dir++) {
	        su3_adjoint( d_fat + 4*d_bck[4*mySite+dir]+dir, d_fatbck + 4*mySite+dir );
	        su3_adjoint( d_lng + 4*d_bck3[4*mySite+dir]+dir, d_lngbck + 4*mySite+dir );
	      }
	    }
	  }); // end of the kernel lambda function
  });   // end of command group
  e_make_back.wait();
  double back_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-back_start).count();
  if (verbose > 1) {
    std::cout << "Time to create back links = " << back_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }
  
  // Dslash benchmark loop
  total_wi = total_even_sites * THREADS_PER_SITE;
  if (verbose > 0) {
    std::cout << "Running dslash loop" << std::endl;
    std::cout << "Setting number of work items to " << total_wi << std::endl;
    std::cout << "Setting workgroup size to " << wgsize << std::endl;
  }
  auto tstart = Clock::now();
  for (int iters=0; iters<iterations+warmups; ++iters) {
    if (iters == warmups) {
      queue.wait();
      tstart = Clock::now();
    } 
    // Dslash kernel
    queue.submit( [&](handler& cgh) {
            cgh.depends_on({e_make_back, e_src, e_fwd, e_fwd3});
	    auto c = accessor<Complx, 1, access::mode::read_write, access::target::local>(wgsize, cgh);
	    // Lambda function defines the kernel scope
	    cgh.parallel_for<class dslash>(program1.get_kernel<dslash>(),
	    nd_range<1> {total_wi, wgsize}, [=](nd_item<1> item) {
	      size_t myThread = item.get_global_id(0);
	      size_t mySite = myThread/THREADS_PER_SITE;

	      //wgsize=48,96 -> must be a multiple of 48 and divisible by total_wi
#if OPTION == 1
	      int k = myThread%4;
	      int row = (myThread/4)%3;
#else
	      int k = (myThread/3)%4;
	      int row = myThread%3;
#endif
              int mat = (myThread/12)%4;

	      int local_id = item.get_local_id(0);

	      if (mySite < total_even_sites) {

                  if (mat == 0) {
                      auto *a = d_fat + mySite*4 + k;
                      auto *b = d_src + d_fwd[4*mySite + k];
                      Complx x = {0.0, 0.0};
                      for(int j=0;j<3;j++){
                        CMULSUM( a->e[row][j] , b->c[j] , x );
                      }
                      c[local_id].real = x.real;
                      c[local_id].imag = x.imag;
                  }
                  else if (mat == 1) {
                      auto *a = d_lng + mySite*4 + k;
      	              auto *b = d_src + d_fwd3[4*mySite + k];
                      Complx x = {0.0, 0.0};
                      for(int j=0;j<3;j++){
                        CMULSUM( a->e[row][j] , b->c[j] , x );
                      }
                      c[local_id].real = x.real;
                      c[local_id].imag = x.imag;
                  }
                  else if (mat == 2) { 
                      auto *a = d_fatbck + mySite*4 + k;
      	              auto *b = d_src + d_bck[4*mySite + k];
                      Complx x = {0.0, 0.0};
                      for(int j=0;j<3;j++){
                        CMULSUM( a->e[row][j] , b->c[j] , x );
                      }
                      c[local_id].real = x.real;
                      c[local_id].imag = x.imag;
                  }
                  else if (mat == 3) {
                      auto *a = d_lngbck + mySite*4 + k;
      	              auto *b = d_src + d_bck3[4*mySite + k];
                      Complx x = {0.0, 0.0};
                      for(int j=0;j<3;j++){
                        CMULSUM( a->e[row][j] , b->c[j] , x );
                      }
                      c[local_id].real = x.real;
                      c[local_id].imag = x.imag;
                  }

                  item.barrier(sycl::access::fence_space::local_space);

                  if( mat == 0) { 
                      c[local_id].real = c[local_id].real + c[local_id+12].real - c[local_id+24].real - c[local_id+36].real;
                      c[local_id].imag = c[local_id].imag + c[local_id+12].imag - c[local_id+24].imag - c[local_id+36].imag;
                  }

                  item.barrier(sycl::access::fence_space::local_space);

                  if( mat == 0 && k == 0) { 
#if OPTION == 1
                      d_dst[mySite].c[row].real = c[local_id].real + c[local_id+1].real + c[local_id+2].real + c[local_id+3].real;
                      d_dst[mySite].c[row].imag = c[local_id].imag + c[local_id+1].imag + c[local_id+2].imag + c[local_id+3].imag;
#else
                      d_dst[mySite].c[row].real = c[local_id].real + c[local_id+3].real + c[local_id+6].real + c[local_id+9].real;
                      d_dst[mySite].c[row].imag = c[local_id].imag + c[local_id+3].imag + c[local_id+6].imag + c[local_id+9].imag;
#endif
                  }
	
	      } // end of if mySite
	    }); // end of the kernel lambda function
    });   // end of command group
    queue.wait();
  } // end of iteration loop
  double ttotal = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-tstart).count();
    
  // Move the result back to the host
  copy_start = Clock::now();
  queue.memcpy(dst.data(), d_dst, dst.size() * sizeof(su3_vector));
  // Copy backward links to host for use in validation
  queue.memcpy(fatbck.data(), d_fatbck, fatbck.size() * sizeof(su3_matrix));
  queue.memcpy(lngbck.data(), d_lngbck, lngbck.size() * sizeof(su3_matrix));
  queue.wait();
  copy_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-copy_start).count();
  if (verbose > 1) {
    std::cout << "Time to offload backward links = " << copy_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }

  free(d_src, queue);
  free(d_dst, queue);    
  free(d_fat, queue);    
  free(d_lng, queue);    
  free(d_fatbck, queue); 
  free(d_lngbck, queue); 

  free(d_fwd, queue);  
  free(d_bck, queue);  
  free(d_fwd3, queue); 
  free(d_bck3, queue); 

  return (ttotal /= 1.0e6);
}
