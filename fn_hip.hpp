#include <hip/hip_runtime.h>
#define THREADS_PER_SITE 48


#define HIPCHECK(cmd) \
  {                     \
    hipError_t error = cmd;  \
    if (error != hipSuccess) {  \
      printf("error: '%s'(%d) at %s:%d\n",\
        hipGetErrorString(error), error, __FILE__, __LINE__); \
      exit(EXIT_FAILURE);      \
    }             \
  }


__global__
void make_back(
    su3_matrix* __restrict__ d_fat,
    su3_matrix* __restrict__ d_lng,
    su3_matrix*       __restrict__ d_fatbck,
    su3_matrix*       __restrict__ d_lngbck,
    const size_t*     __restrict__ d_bck,
    const size_t*     __restrict__ d_bck3,
    int         total_even_sites)
{
  int myThread = blockDim.x * blockIdx.x + threadIdx.x;
  int mySite = myThread;
  if (mySite < total_even_sites) {
    for(int dir = 0; dir < 4; dir++) {
      su3_matrix *a = d_fat + 4*d_bck[4*mySite+dir]+dir;
      su3_matrix *b = d_fatbck + 4*mySite+dir;
      for(int i=0;i<3;i++)for(int j=0;j<3;j++){
          CONJG( a->e[j][i], b->e[i][j] );
      }

      a = d_lng + 4*d_bck3[4*mySite+dir]+dir;
      b = d_lngbck + 4*mySite+dir;
      for(int i=0;i<3;i++)for(int j=0;j<3;j++){
          CONJG( a->e[j][i], b->e[i][j] );
      }
    }
  }
}


__global__
void dslash(
    const su3_matrix* __restrict__ d_fat,
    const su3_matrix* __restrict__ d_lng,
    const su3_matrix* __restrict__ d_fatbck,
    const su3_matrix* __restrict__ d_lngbck,
    const su3_vector* __restrict__ d_src,
    su3_vector*       __restrict__ d_dst,
    const size_t*     __restrict__ d_fwd,
    const size_t*     __restrict__ d_fwd3,
    const size_t*     __restrict__ d_bck,
    const size_t*     __restrict__ d_bck3,
    int               total_even_sites)
{

  HIP_DYNAMIC_SHARED(Complx, v);


  int myThread = blockDim.x * blockIdx.x + threadIdx.x;
  int mySite = myThread/THREADS_PER_SITE;


  //threadsPerBlock=48,96 -> must be a multiple of 48 and able to divide total_even_sites * THREADS_PER_SITE
#if OPTION == 1
  //option 1
  //originally for threadsPerBlock=4,8,12,16,24,32,48,64,96,128 -> must be a multiple of 4
  int k = myThread%4;
  int row = (myThread/4)%3;
#else
  //option 2
  //threadsPerBlock=12,24,48,96 -> must be a multiple of 12
  int k = (myThread/3)%4;
  int row = myThread%3;
#endif
  int mat = (myThread/12)%4;


  if (mySite < total_even_sites) {

    v[threadIdx.x] = {0., 0.};

    if (mat == 0) {
      auto *a = d_fat + mySite*4 + k;
      auto *b = d_src + d_fwd[4*mySite + k];
      Complx x = {0.0, 0.0};
      for(int j=0;j<3;j++){
        CMULSUM( a->e[row][j] , b->c[j] , x );
      }
      v[threadIdx.x].real = x.real;
      v[threadIdx.x].imag = x.imag;
    }

    if (mat == 1) {
      auto *a = d_lng + mySite*4 + k;
      auto *b = d_src + d_fwd3[4*mySite + k];
      Complx x = {0.0, 0.0};
      for(int j=0;j<3;j++){
        CMULSUM( a->e[row][j] , b->c[j] , x );
      }
      v[threadIdx.x].real = x.real;
      v[threadIdx.x].imag = x.imag;
    }

    if (mat == 2) {
      auto *a = d_fatbck + mySite*4 + k;
      auto *b = d_src + d_bck[4*mySite + k];
      Complx x = {0.0, 0.0};
      for(int j=0;j<3;j++){
        CMULSUM( a->e[row][j] , b->c[j] , x );
      }
      v[threadIdx.x].real = x.real;
      v[threadIdx.x].imag = x.imag;
    }

    if (mat == 3) {
      auto *a = d_lngbck + mySite*4 + k;
      auto *b = d_src + d_bck3[4*mySite + k];
      Complx x = {0.0, 0.0};
      for(int j=0;j<3;j++){
        CMULSUM( a->e[row][j] , b->c[j] , x );
      }
      v[threadIdx.x].real = x.real;
      v[threadIdx.x].imag = x.imag;
    }


    __syncthreads();

    if( mat == 0) {
      v[threadIdx.x].real = v[threadIdx.x].real + v[threadIdx.x+12].real - v[threadIdx.x+24].real - v[threadIdx.x+36].real;
      v[threadIdx.x].imag = v[threadIdx.x].imag + v[threadIdx.x+12].imag - v[threadIdx.x+24].imag - v[threadIdx.x+36].imag;
    }

    __syncthreads();

    if( k == 0) {
#if OPTION == 1
      //option 1
      d_dst[mySite].c[row].real = v[threadIdx.x].real + v[threadIdx.x+1].real + v[threadIdx.x+2].real + v[threadIdx.x+3].real;
      d_dst[mySite].c[row].imag = v[threadIdx.x].imag + v[threadIdx.x+1].imag + v[threadIdx.x+2].imag + v[threadIdx.x+3].imag;
#else
      //option 2
      d_dst[mySite].c[row].real = v[threadIdx.x].real + v[threadIdx.x+3].real + v[threadIdx.x+6].real + v[threadIdx.x+9].real;
      d_dst[mySite].c[row].imag = v[threadIdx.x].imag + v[threadIdx.x+3].imag + v[threadIdx.x+6].imag + v[threadIdx.x+9].imag;
#endif
    }

  }

}


double dslash_fn(
  const std::vector<su3_vector> &src,
        std::vector<su3_vector> &dst,
  const std::vector<su3_matrix> &fat,
  const std::vector<su3_matrix> &lng,
        std::vector<su3_matrix> &fatbck,
        std::vector<su3_matrix> &lngbck,
  size_t *fwd, size_t *bck, size_t *fwd3, size_t *bck3,
  const size_t iterations,
  size_t threadsPerBlock )
{



  if (threadsPerBlock == 0)
  threadsPerBlock = THREADS_PER_SITE;



  // Device initialization
  int deviceCount;
  hipGetDeviceCount(&deviceCount);
  if (deviceCount == 0) {
    printf("ERROR: No devices found\n");
    exit(1);
  }

  hipDeviceProp_t *prop;
  if (verbose >= 3) {
    for (int i = 0; i < deviceCount; i++) {
      hipGetDeviceProperties(prop, i);
      printf("Located device %d: %s\n", i, prop->name);
    }
  }

  int total_sites = sites_on_node;
  int total_even_sites = even_sites_on_node;

  auto copy_start = Clock::now();
  // allocate device memory
  hipError_t Err;
  su3_vector *d_src, *d_dst;
  su3_matrix *d_fat, *d_lng, *d_fatbck, *d_lngbck;
  HIPCHECK( hipMalloc((void **)&d_src, total_sites * 1 * sizeof(su3_vector)) );
  HIPCHECK( hipMalloc((void **)&d_dst, total_sites * 1 * sizeof(su3_vector)) );
  HIPCHECK( hipMalloc((void **)&d_fat, total_sites * 4 * sizeof(su3_matrix)) );
  HIPCHECK( hipMalloc((void **)&d_lng, total_sites * 4 * sizeof(su3_matrix)) );
  HIPCHECK( hipMalloc((void **)&d_fatbck, total_sites * 4 * sizeof(su3_matrix)) );
  HIPCHECK( hipMalloc((void **)&d_lngbck, total_sites * 4 * sizeof(su3_matrix)) );

  size_t *d_fwd, *d_bck, *d_fwd3, *d_bck3;
  HIPCHECK( hipMalloc((void **)&d_fwd, total_sites * 4 * sizeof(size_t)) );
  HIPCHECK( hipMalloc((void **)&d_bck, total_sites * 4 * sizeof(size_t)) );
  HIPCHECK( hipMalloc((void **)&d_fwd3, total_sites * 4 * sizeof(size_t)) );
  HIPCHECK( hipMalloc((void **)&d_bck3, total_sites * 4 * sizeof(size_t)) );

  size_t bytes  = sizeof(size_t)*total_sites*4;
  HIPCHECK(hipMemcpy(d_fat, fat.data(), fat.size() * sizeof(su3_matrix),  hipMemcpyHostToDevice));
  HIPCHECK(hipMemcpy(d_lng, lng.data(), lng.size() * sizeof(su3_matrix),  hipMemcpyHostToDevice));
  HIPCHECK(hipMemcpy(d_bck, bck, bytes,  hipMemcpyHostToDevice));
  HIPCHECK(hipMemcpy(d_bck3, bck3, bytes,  hipMemcpyHostToDevice));

  HIPCHECK(hipMemcpy(d_src, src.data(), src.size() * sizeof(su3_vector),  hipMemcpyHostToDevice));
  HIPCHECK(hipMemcpy(d_fwd, fwd, bytes,  hipMemcpyHostToDevice));
  HIPCHECK(hipMemcpy(d_fwd3, fwd3, bytes,  hipMemcpyHostToDevice));

  double copy_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-copy_start).count();
  if (verbose > 1) {
    std::cout << "Time to offload input data = " << copy_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }

  // Create backward links on the device
  size_t blocksPerGrid = total_even_sites;
  //int blocksPerGrid = total_even_sites + 0.999999;
  if (verbose > 1) {
    std::cout << "Creating backward links"  << std::endl;
    std::cout << "Setting number of blocks " << blocksPerGrid << std::endl;
    std::cout << "Setting ThreadBlock size to " << 1 << std::endl;
  }
  auto back_start = Clock::now();
  hipLaunchKernelGGL(make_back, dim3(blocksPerGrid), dim3(1), 0, 0,
                     d_fat, d_lng, d_fatbck, d_lngbck, d_bck, d_bck3, total_even_sites);
  hipDeviceSynchronize();
  double back_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-back_start).count();
  HIPCHECK(hipGetLastError(), "make_back kernel Failed");
  if (verbose > 1) {
    std::cout << "Time to create back links = " << back_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }

  // Dslash benchmark loop
  blocksPerGrid = (total_even_sites * THREADS_PER_SITE)/(double)threadsPerBlock;

  if (verbose > 0) {
    std::cout << "Running dslash loop" << std::endl;
    std::cout << "Setting total number of threads " << total_even_sites * THREADS_PER_SITE << std::endl;
    std::cout << "Setting number of blocks to " << blocksPerGrid << std::endl;
    std::cout << "Setting ThreadBlock size to " << threadsPerBlock << std::endl;
  }
  auto tstart = Clock::now();
  for (int iters=0; iters<iterations+warmups; ++iters) {
    if (iters == warmups) {
      hipDeviceSynchronize();
      tstart = Clock::now();
    }
    // Dslash kernel
    hipLaunchKernelGGL(dslash, dim3(blocksPerGrid), dim3(threadsPerBlock), threadsPerBlock * sizeof(Complx), 0,
                       d_fat, d_lng, d_fatbck, d_lngbck, d_src, d_dst, d_fwd, d_fwd3, d_bck, d_bck3, total_even_sites);
    hipDeviceSynchronize();
  } // end of iteration loop
  double ttotal = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-tstart).count();
  HIPCHECK(hipGetLastError(), "dslash kernel Failed");

  // Move the result back to the host
  copy_start = Clock::now();
  HIPCHECK(hipMemcpy(dst.data(), d_dst, dst.size() * sizeof(su3_vector), hipMemcpyDeviceToHost));
  // Copy backward links to host for use in validation
  HIPCHECK(hipMemcpy(fatbck.data(), d_fatbck, fatbck.size() * sizeof(su3_matrix), hipMemcpyDeviceToHost));
  HIPCHECK(hipMemcpy(lngbck.data(), d_lngbck, lngbck.size() * sizeof(su3_matrix), hipMemcpyDeviceToHost));
  copy_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-copy_start).count();
  if (verbose > 1) {
    std::cout << "Time to offload backward links = " << copy_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }

  HIPCHECK(hipFree(d_src));
  HIPCHECK(hipFree(d_dst));
  HIPCHECK(hipFree(d_fat));
  HIPCHECK(hipFree(d_lng));
  HIPCHECK(hipFree(d_fatbck));
  HIPCHECK(hipFree(d_lngbck));

  HIPCHECK(hipFree(d_fwd));
  HIPCHECK(hipFree(d_bck));
  HIPCHECK(hipFree(d_fwd3));
  HIPCHECK(hipFree(d_bck3));

  return (ttotal /= 1.0e6);

}
