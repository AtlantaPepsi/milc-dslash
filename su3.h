#ifndef _SU3_H
#define _SU3_H
/* Adapted from su3.h in MILC version 7 */

/* generic precision complex number definition */
/* specific for float complex */
typedef struct {   
  float real;
  float imag; 
} fcomplex;  

/* specific for double complex */
typedef struct {
   double real;
   double imag;
} dcomplex;

typedef struct { fcomplex e[3][3]; } fsu3_matrix;
typedef struct { fcomplex c[3]; } fsu3_vector;

typedef struct { dcomplex e[3][3]; } dsu3_matrix;
typedef struct { dcomplex c[3]; } dsu3_vector;

#if (PRECISION==1)
  #define su3_matrix    fsu3_matrix
  #define su3_vector    fsu3_vector
  #define Real          float
  #define Complx        fcomplex
#else
  #define su3_matrix    dsu3_matrix
  #define su3_vector    dsu3_vector
  #define Real          double
  #define Complx        dcomplex
#endif  /* PRECISION */

/*  c = a + b */
#define CADD(a,b,c)    { (c).real = (a).real + (b).real;  \
                         (c).imag = (a).imag + (b).imag; }
/*  c = a - b */
#define CSUB(a,b,c)    { (c).real = (a).real - (b).real;  \
                         (c).imag = (a).imag - (b).imag; }
/*  c += a * b */
#define CMULSUM(a,b,c) { (c).real += (a).real*(b).real - (a).imag*(b).imag; \
                         (c).imag += (a).real*(b).imag + (a).imag*(b).real; }
/*  c = a * b */
#define CMUL(a,b,c)    { (c).real = (a).real*(b).real - (a).imag*(b).imag; \
                         (c).imag = (a).real*(b).imag + (a).imag*(b).real; }
/*  a += b    */
#define CSUM(a,b)      { (a).real += (b).real; (a).imag += (b).imag; }
/*   b = a*   */
#define CONJG(a,b)     { (b).real = (a).real; (b).imag = -(a).imag; }

//--------------------------------------------------------------------------------
inline void su3_adjoint( const su3_matrix *a, su3_matrix *b ){
  int i,j;
  for(i=0;i<3;i++)for(j=0;j<3;j++){
      CONJG( a->e[j][i], b->e[i][j] );
    }
}
//--------------------------------------------------------------------------------
inline void mult_su3_mat_vec( const su3_matrix *a, const su3_vector *b, 
			      su3_vector *c  ){
  int i,j;
  for(i=0;i<3;i++){
    Complx x = {0.0, 0.0};
    for(j=0;j<3;j++){
      CMULSUM( a->e[i][j] , b->c[j] , x );
    }
    c->c[i] = x;
  }
}
//--------------------------------------------------------------------------------
inline void mult_su3_mat_vec_sum( const su3_matrix *a, const su3_vector *b, 
				  su3_vector *c ){
  int i,j;
  for(i=0;i<3;i++){
    Complx x = {0.0, 0.0};
    for(j=0;j<3;j++){
      CMULSUM( a->e[i][j] , b->c[j] , x );
    }
    c->c[i].real += x.real;
    c->c[i].imag += x.imag;
  }
}
//--------------------------------------------------------------------------------
inline void mult_su3_mat_vec_sum_4dir( su3_matrix *a, su3_vector *b0,
       su3_vector *b1, su3_vector *b2, su3_vector *b3, 
       su3_vector *c  ){
    mult_su3_mat_vec(       a,b0,c );
    mult_su3_mat_vec_sum( ++a,b1,c );
    mult_su3_mat_vec_sum( ++a,b2,c );
    mult_su3_mat_vec_sum( ++a,b3,c );
}
//--------------------------------------------------------------------------------
inline void add_su3_vector( const su3_vector *a, const su3_vector *b, su3_vector *c ){
  int i;
  for(i=0;i<3;i++){
    CADD( a->c[i], b->c[i], c->c[i] );
  }
}
//--------------------------------------------------------------------------------
inline void sub_su3_vector( const su3_vector *a, const su3_vector *b, su3_vector *c ){
  int i;
  for(i=0;i<3;i++){
    CSUB( a->c[i], b->c[i], c->c[i] );
  }
}
//--------------------------------------------------------------------------------
#include <stdio.h>

inline void dumpmat( su3_matrix *m ){
  int i,j;
  for(i=0;i<3;i++){
	  for(j=0;j<3;j++)
      printf("(%.2e,%.2e)\t", m->e[i][j].real,m->e[i][j].imag);
	  printf("\n");
  }
  printf("\n");
}
//--------------------------------------------------------------------------------

#endif  /* _SU3_H */

