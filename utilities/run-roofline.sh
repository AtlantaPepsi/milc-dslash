#!/bin/bash
set -x

exe=./dslash_dpcpp.x
threads=128

skip=10
count=1
metrics="--metrics dram__bytes.sum,dram__bytes_read.sum,dram__bytes_write.sum,sm__sass_thread_inst_executed_op_dadd_pred_on.sum,sm__sass_thread_inst_executed_op_dfma_pred_on.sum,sm__sass_thread_inst_executed_op_dmul_pred_on.sum"

srun nv-nsight-cu-cli -s $skip -c $count $metrics $exe $threads

